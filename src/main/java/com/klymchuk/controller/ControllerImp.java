package com.klymchuk.controller;

import com.klymchuk.model.House;
import com.klymchuk.model.Model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MVC controller.
 */
public class ControllerImp implements Controller {

    /**
     * model.
     */
    private Model model;

    /**
     * create House object.
     */
    public ControllerImp() {
        model = new House();
    }

    /**
     * count all power.
     */
    @Override
    public final void getAllPower() {
        System.out.println(model.getAllPower());
    }

    /**
     * sort electric appliances by power.
     */
    @Override
    public final void sort() {
        model.sort();
    }

    /**
     * show all electric appliances.
     */
    @Override
    public final void showElectricalAppliances() {
        model.showElectricalAppliances();
    }

    /**
     * show commands menu.
     */
    @Override
    public final void menu() {
        model.menu();
    }

    /**
     * write electric appliance in the file.
     *
     * @param command regular command
     */
    @Override
    public final void inTheFile(final String command) {
        String regex = "\\s*tofile\\s+(?<name>\\w+)\\s+"
                + "(?<connection>\\w+)\\s+(?<power>\\d+\\s*)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(command);
        String name = null;
        String connection = null;
        boolean plugged;
        double power = 0.0;
        if (matcher.find()) {
            name = matcher.group("name");
            connection = (matcher.group("connection"));
            power = Double.parseDouble((matcher.group("power")));
        }
        plugged = connection.equals("on");
        model.inTheFile(name, plugged, power);
    }

    /**
     * read electric appliance from file.
     */
    @Override
    public final void fromTheFile() {
        model.fromTheFile();
    }

    /**
     * switch or off plugged to socket item.
     *
     * @param command regular command
     */
    @Override
    public final void switchOrOffDevice(final String command) {
        String regex = "\\s*change\\s+connection\\s+"
                + "(?<name>\\w+)\\s+(?<connection>\\w+)\\s*";
        boolean plugged;
        String connection = null, name = null;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) {
            name = matcher.group("name");
            connection = matcher.group("connection");
        }
        plugged = connection.equals("on");
        model.switchOrOffDevice(name, plugged);
    }

    /**
     * Find a device that meets a given range of parameters.
     *
     * @param command regular command
     */
    @Override
    public final void findElectricalAppliancesByPower(final String command) {
        String regex = "\\s*range\\s+by\\s+power\\s+"
                + "(?<from>\\d+)\\s+(?<to>\\d+)\\s*";
        double minPower = 0.0, maxPower = 0.0;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) {
            minPower = Double.parseDouble(matcher.group("from"));
            maxPower = Double.parseDouble(matcher.group("to"));
        }
        model.findElectricalAppliancesByPower(minPower, maxPower);
    }

    /**
     * show all elements that have name which you wrote.
     *
     * @param command regular command
     */
    @Override
    public final void findElectricalAppliancesByName(final String command) {
        String regex = "\\s*range\\s+by\\s+name\\s+(?<name>\\w+)";
        String name = null;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) {
            name = matcher.group("name");
        }
        model.findElectricalAppliancesByName(name);
    }

    /**
     * show all elements that have connection which you wrote.
     *
     * @param command regular command
     */
    @Override
    public final void findElectricalAppliancesByConnection(
            final String command) {
        String regex = "\\s*range\\s+by\\s+connection\\s+(?<connection>\\w+)";
        boolean plugged;
        String connection = null;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(command);
        if (matcher.find()) {
            connection = matcher.group("connection");
        }
        plugged = connection.equals("on");
        model.findElectricalAppliancesByConnection(plugged);
    }
}
