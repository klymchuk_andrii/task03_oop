package com.klymchuk.controller;

/**
 * This interface describes functions
 * which do controller to interact with model.
 */
public interface Controller {
    /**
     * count all power.
     */
    void getAllPower();

    /**
     * sort electric appliances by power.
     */
    void sort();

    /**
     * show all electric appliances.
     */
    void showElectricalAppliances();

    /**
     * show commands menu.
     */
    void menu();

    /**
     * write electric appliance in the file.
     *
     * @param command regular command
     */
    void inTheFile(String command);

    /**
     * read electric appliance from file.
     */
    void fromTheFile();

    /**
     * switch or off plugged to socket item.
     *
     * @param command regular command
     */
    void switchOrOffDevice(String command);

    /**
     * Find a device that meets a given range of parameters.
     *
     * @param command regular command
     */
    void findElectricalAppliancesByPower(String command);

    /**
     * show all elements that have name which you wrote.
     *
     * @param command regular command
     */
    void findElectricalAppliancesByName(String command);

    /**
     * show all elements that have connection which you wrote.
     *
     * @param command regular command
     */
    void findElectricalAppliancesByConnection(String command);
}
