package com.klymchuk;

import com.klymchuk.view.View;

/**
 * The Main program class.
 */
public class Application {
    /**
     * @param args command line values
     * @author Andrii Klymchuk
     */
    public static void main(String[] args) {
        new View().show();
    }
}
