package com.klymchuk.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * MVC model.
 */
public class House implements Model {

    /**
     * list of electrical appliances.
     */
    private List<ElectricalAppliances> electricalAppliances;

    /**
     * create array list.
     */
    public House() {
        electricalAppliances = new ArrayList<>();
    }

    /**
     * count all power.
     */
    @Override
    public final double getAllPower() {
        double allPower = 0.0;
        for (int i = 0; i < electricalAppliances.size(); i++) {
            if (electricalAppliances.get(i).getPluggedToSocket()) {
                allPower += electricalAppliances.get(i).getPower();
            }
        }
        return allPower;
    }

    /**
     * check if device >= minPower and <= maxPower.
     *
     * @param electricalDevice electrical appliance
     * @param maxPower         maximum power
     * @param minPower         minimum power
     */
    @Override
    public final boolean findElectricalAppliances(
            final ElectricalAppliances electricalDevice,
            final double minPower, final double maxPower) {
        return electricalDevice.getPower() >= minPower
                && electricalDevice.getPower() <= maxPower;
    }

    /**
     * sort electric appliances by power.
     */
    @Override
    public final void sort() {
        Collections.sort(electricalAppliances,
                new Comparator<ElectricalAppliances>() {
                    public int compare(final ElectricalAppliances o1,
                                       final ElectricalAppliances o2) {
                        return Double.compare(o1.getPower(), o2.getPower());
                    }
                });
    }

    /**
     * show all electric appliances.
     */
    @Override
    public final void showElectricalAppliances() {
        if (!electricalAppliances.isEmpty()) {
            for (ElectricalAppliances electricalAppliance
                    : electricalAppliances) {
                System.out.println(electricalAppliance);
            }
        } else {
            System.out.println("in file have not device");
        }

    }

    /**
     * show commands menu.
     */
    @Override
    public final void menu() {
        System.out.println("show - print your items");
        System.out.println("tofile (name) (connection(switch or off)"
                + " (power) - write items in the file");
        System.out.println("read from file - read your elements from file :)");
        System.out.println("sort - sort you items by power");
        System.out.println("allpower - count all power");
        System.out.println("range by power (minPower) (maxPower) - "
                + "Find a device that meets a given range of parameters");
        System.out.println("range by name (name)- show all elements"
                + " that have name which you wrote");
        System.out.println("range by connection (connection (on or Off))- "
                + "show all elements that have connection which you wrote");
        System.out.println("change connection (name) (connection (on or off) -"
                + " switch or off plugged to socket item");
        System.out.println("exit - complete the program");
    }

    /**
     * write electric appliance in the file.
     *
     * @param name    name of electrical appliance
     * @param plugged true if electrical appliance
     *                plugged to socket
     * @param power   power of electrical appliance
     */
    @Override
    public final void inTheFile(final String name,
                                final boolean plugged,
                                final double power) {
        electricalAppliances.add(new ElectricalAppliances(name,
                plugged, power));
        GetForFile get = new GetForFile(electricalAppliances);
        try {
            FileOutputStream fo = new FileOutputStream(
                    "C:\\Users\\Andrii\\IdeaProjects"
                            + "\\task03\\electric_appliances.txt");
            ObjectOutputStream serial = new ObjectOutputStream(fo);
            serial.writeObject(get);
            fo.close();
            System.out.println("You wrote to the file");
        } catch (Exception e) {
            System.out.println("Error");
            System.out.println(e);
        }
    }

    /**
     * read electric appliance from file.
     */
    @Override
    public final void fromTheFile() {
        try {
            FileInputStream fin = new FileInputStream(
                    "C:\\Users\\Andrii\\IdeaProjects"
                            + "\\task03\\electric_appliances.txt");
            ObjectInputStream ois = new ObjectInputStream(fin);
            GetForFile get = (GetForFile) ois.readObject();
            electricalAppliances = get.getForFile();
            fin.close();
            System.out.println("You read items,"
                    + "if you want see that print a (show)");
        } catch (Exception e) {
            System.out.println("error");
            System.out.println(e);
        }
    }

    /**
     * switch or off plugged to socket item.
     *
     * @param name    name of electrical appliance
     * @param plugged true if electrical appliance
     *                plugged to socket
     */
    @Override
    public final void switchOrOffDevice(final String name,
                                        final boolean plugged) {
        for (ElectricalAppliances electricalAppliance : electricalAppliances) {
            if (name.equals(electricalAppliance.getName())) {
                electricalAppliance.setPluggedToSocket(plugged);
                System.out.println(electricalAppliance);
            }

        }
    }

    /**
     * Find a device that meets a given range of parameters.
     *
     * @param maxPower maximum power
     * @param minPower minimum power
     */
    @Override
    public final void findElectricalAppliancesByPower(final double minPower,
                                                      final double maxPower) {
        for (int i = 0; i < electricalAppliances.size(); i++) {
            if (findElectricalAppliances(electricalAppliances.get(i),
                    minPower, maxPower)) {
                System.out.println(electricalAppliances.get(i));
            }
        }
    }

    /**
     * show all elements that have name which you wrote.
     *
     * @param name name of electrical appliance
     */
    @Override
    public final void findElectricalAppliancesByName(final String name) {

        for (int i = 0; i < electricalAppliances.size(); i++) {
            if ((electricalAppliances.get(i).getName()).equals(name)) {
                System.out.println(electricalAppliances.get(i));
            }
        }
    }

    /**
     * show all elements that have connection which you wrote.
     *
     * @param plugged true if electrical appliance
     *                plugged to socket
     */
    @Override
    public final void findElectricalAppliancesByConnection(
            final boolean plugged) {
        for (int i = 0; i < electricalAppliances.size(); i++) {
            if (electricalAppliances.get(i).getPluggedToSocket() == plugged) {
                System.out.println(electricalAppliances.get(i));
            }
        }
    }
}
