package com.klymchuk.model;

import java.io.Serializable;

/**
 * Describes electrical appliance
 * its name,plugging to socket.
 */
public class ElectricalAppliances implements Serializable {
    /**
     * Describes name of electrical appliance.
     */
    private String name;
    /**
     * Describes if electrical appliance
     * plugged to socket.
     */
    private boolean isPluggedToSocket;
    /**
     * Describes power of electrical appliance.
     */
    private double power;

    /**
     * @param nameParam       name of electrical appliance
     * @param pluggedToSocket true if electrical appliance
     *                        plugged to socket
     * @param powerParam      power of electrical appliance
     */
    ElectricalAppliances(final String nameParam,
                         final boolean pluggedToSocket,
                         final double powerParam) {
        this.name = nameParam;
        this.isPluggedToSocket = pluggedToSocket;
        this.power = powerParam;
    }

    /**
     * @return name of electrical appliance
     */
    public final String getName() {
        return name;
    }

    /**
     * @param pluggedToSocket true if electrical appliance
     *                        plugged to socket
     */
    public final void setPluggedToSocket(final boolean pluggedToSocket) {
        this.isPluggedToSocket = pluggedToSocket;
    }

    /**
     * @return true if electrical appliance
     * plugged to socket
     */
    public final boolean getPluggedToSocket() {
        return isPluggedToSocket;
    }

    /**
     * @return power of electrical appliance
     */
    public final double getPower() {
        return power;
    }

    /**
     * @return text which describes electrical appliance
     */
    public final String toString() {
        return (name + " " + (isPluggedToSocket ? "plugged to socket outlet"
                : "no plugged to socket outlet") + " ,power: " + power);
    }

}
