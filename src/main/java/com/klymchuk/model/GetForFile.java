package com.klymchuk.model;

import java.io.Serializable;
import java.util.List;

/**
 *This class implements
 * interface Serializable.
 */
public class GetForFile implements Serializable {
    /**
     *list of electrical appliances.
     */
    private List<ElectricalAppliances> list;

    /**
     *@return  list of electrical appliances
     */
    public final List<ElectricalAppliances> getForFile() {
        return list;
    }

    /**
     *@param listParam list of electrical appliances
     */
    public GetForFile(final List<ElectricalAppliances> listParam) {
        this.list = listParam;
    }
}
