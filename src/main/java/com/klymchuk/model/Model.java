package com.klymchuk.model;

/**
 * This interface describes functions
 * which do house to interact with model.
 */
public interface Model {
    /**
     * @return counted all power.
     */
    double getAllPower();

    /**
     * check if device >= minPower and <= maxPower.
     *
     * @param electricalDevice electrical appliance
     * @param maxPower         maximum power
     * @param minPower         minimum power
     * @return device which >= minPower and <= maxPower
     */
    boolean findElectricalAppliances(ElectricalAppliances electricalDevice,
                                     double minPower, double maxPower);

    /**
     * sort electric appliances by power.
     */
    void sort();

    /**
     * show all electric appliances.
     */
    void showElectricalAppliances();

    /**
     * show commands menu.
     */
    void menu();

    /**
     * write electric appliance in the file.
     *
     * @param name    name of electrical appliance
     * @param plugged true if electrical appliance
     *                plugged to socket
     * @param power   power of electrical appliance
     */
    void inTheFile(String name, boolean plugged, double power);

    /**
     * read electric appliance from file.
     */
    void fromTheFile();

    /**
     * switch or off plugged to socket item.
     *
     * @param name    name of electrical appliance
     * @param plugged true if electrical appliance
     *                plugged to socket
     */
    void switchOrOffDevice(String name, boolean plugged);

    /**
     * Find a device that meets a given range of parameters.
     *
     * @param maxPower maximum power
     * @param minPower minimum power
     */
    void findElectricalAppliancesByPower(double minPower, double maxPower);

    /**
     * show all elements that have name which you wrote.
     *
     * @param name name of electrical appliance
     */
    void findElectricalAppliancesByName(String name);

    /**
     * show all elements that have connection which you wrote.
     *
     * @param plugged true if electrical appliance
     *                plugged to socket
     */
    void findElectricalAppliancesByConnection(boolean plugged);
}
