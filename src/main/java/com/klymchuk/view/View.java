package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImp;

import java.util.Scanner;

/**
 * This class
 * interacts with the user.
 * It reads commands and calls
 * appropriate controller methods.
 */
public class View {
    /**
     * read data entered user.
     */
    private Scanner scanName = new Scanner(System.in);

    /**
     * controller.
     */
    private Controller controller;

    /**
     * create ControllerImp object.
     */
    public View() {
        controller = new ControllerImp();
    }

    /**
     * It reads commands and calls
     * appropriate controller methods.
     */
    public final void show() {
        System.out.println("If you don't know command , write  'help' ");
        String command;
        while (true) {
            command = scanName.nextLine();
            if (command.matches("\\s*help\\s*")) {
                controller.menu();
            } else if (command.matches("\\s*tofile\\s+(?<name>\\w+)\\s+"
                    + "(?<connection>\\w+)\\s+(?<power>\\d+\\s*)")) {
                controller.inTheFile(command);
            } else if (command.matches("\\s*read\\s+from\\s+file\\s*")) {
                controller.fromTheFile();
            } else if (command.matches("\\s*sort\\s*")) {
                controller.sort();
                System.out.println("You sort electrical appliances by power");
            } else if (command.matches("\\s*range\\s+by\\s+name\\s+"
                    + "(?<name>\\w+)")) {
                controller.findElectricalAppliancesByName(command);
            } else if (command.matches("\\s*range\\s+by\\s+connection\\s+"
                    + "(?<connection>\\w+)")) {
                controller.findElectricalAppliancesByConnection(command);
            } else if (command.matches("\\s*allpower\\s*")) {
                controller.getAllPower();
            } else if (command.matches("\\s*range\\s+by\\s+power\\s+"
                    + "(?<from>\\d+)\\s+(?<to>\\d+)\\s*")) {
                controller.findElectricalAppliancesByPower(command);
            } else if (command.matches("\\s*show\\s*")) {
                controller.showElectricalAppliances();
            } else if (command.matches("\\s*change\\s+connection\\s+"
                    + "(?<name>\\w+)\\s+(?<connection>\\w+)\\s*")) {
                controller.switchOrOffDevice(command);
            } else if (command.matches("\\s*exit\\s*")) {
                break;
            } else {
                System.out.println("This command don't exist");
            }
        }
    }
}
